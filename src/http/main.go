package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	item "gitlab.com/pnkfd/cupom/src/models" //importa o item do pacote models
)

//############################### Structs #################################
type Autogenerated struct { //A resposta do "searchAll" do Twitter vai ser incorporada nessa struct
	Data []struct {
		ID       string    `json:"id"`
		Text     string    `json:"text"`
		CreateAt time.Time `json:"created_at"`
	} `json:"data"`
	Meta struct {
		NewestID    string `json:"newest_id"`
		OldestID    string `json:"oldest_id"`
		ResultCount int    `json:"result_count"`
		NextToken   string `json:"next_token"`
	} `json:"meta"`
}
type SingleTweet struct { //Struct utilizada quando buscarmos apenas 1 tweet
	Data struct {
		CreatedAt time.Time `json:"created_at"`
		ID        string    `json:"id"`
		Text      string    `json:"text"`
	} `json:"data"`
}

type DynamoItem struct { //Struct para inserir na tabela do Dynamo
	ID   string    `json:"id"`
	Text string    `json:"text"`
	Date time.Time `json:"created_at"`
}

//####### Variáveis do Lambda
var chat_id = os.Getenv("chat_id")
var bot_id = os.Getenv("bot_id")

//Essas strings abaixo são os textos buscados no Tweets, se achar umas dessas, manda no telegram
var palavras = os.Getenv("palavras") //variáveis
var bearer = os.Getenv("bearer")

var t1 time.Time                                    //agora
var loc, _ = time.LoadLocation("America/Sao_Paulo") //time zone para converter na hora de inserir no dynammo

//Executa o lambda
func main() {
	lambda.Start(parseStruct)
}

// Handler que retorna todos os itens da tabela
func AllHander(w http.ResponseWriter, r *http.Request) {
	// i := item.SearchAll()
	// w.Header().Add("Content-Type", "application/json") // resposta
	// w.WriteHeader(200)
	// w.Write(i)

}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	temp := template.Must(template.ParseFiles("../templates/index.html", "../templates/temp.html"))
	Lista := item.SearchAll()
	temp.ExecuteTemplate(w, "temp.html", Lista)
}

//############ doQUery faz do tweet mais antigo (until_id) para trás ############
func doQuery(until_id string) string {

	// ################################## Request #################################3
	client := &http.Client{} // inicia o client
	// bearer_token, err := os.ReadFile("/home/user/projetos/gitlab.com/pnkfd/cupom/pass") // pega o BEARER TOKEN do twitter
	// if err != nil {
	// 	log.Println(err)
	// }

	//######################## Builda a Query
	query := "https://api.twitter.com/2/tweets/search/recent?"
	query += "until_id=" + until_id + "&"
	query += "max_results=50&"
	query += "tweet.fields=created_at&"
	query += "query=" + url.QueryEscape("Cupom desconto -pack -p4ck -is:reply -is:retweet -from:cupom")

	//#########Prepara o request
	req, err1 := http.NewRequest("GET", query, nil)
	if err1 != nil {
		log.Println(err1)
	}

	req.Header.Add("Authorization", "Bearer "+string(bearer)) //seta o header passando o token
	//fmt.Println(string(bearer_token))                               //debug
	resp, err2 := client.Do(req) // Faz a requisição

	if err2 != nil {
		log.Println(err2)
	}

	defer resp.Body.Close() //fecha no fim

	body, _ := ioutil.ReadAll(resp.Body) // Lê a resposta
	sb := string(body)                   // Converte a resposta para string
	//fmt.Println(sb)               // debug

	return sb // retorna uma string com o JSON da resposta
}

func parseStruct() {
	query := doQuery(GetMaisNovo()) //ID inicial, é por onde começa as buscas

	complete_struct := Autogenerated{}                                      // inicializa a struct que vai ser enviada para o DYNAMMO
	if err := json.Unmarshal([]byte(query), &complete_struct); err != nil { // Faz o unmarshall da resposta para o ponteiro da struct Autogenerated{}
		log.Println(err) // debug
	}

	//################ DynamoDB sessão ################
	sess := session.Must(session.NewSessionWithOptions(session.Options{ //inicia a sessão com os dados de ./aws/config
		SharedConfigState: session.SharedConfigEnable,
	}))
	//Create DynamoDB client
	svc := dynamodb.New(sess)
	tableName := "cupom" //nome da table no dynammo

	//############### Debug - Apenas para visualizar quantos resultados a query trouxe ###########
	count := complete_struct.Meta.ResultCount // Salva o primeiro ResultCount
	fmt.Println("############################# Count Query 1:", count)

	//dados := []string{"livro", "ssd", "placa de video", "amazon", "express", "GUIDE30"}

	for { //itera até um "break"

		for _, stream := range complete_struct.Data { //Itera sobre o array "Data", onde ficam o "text" e "created_at",
			// Esses dados mais o ID que serão gravados na tabela

			if contains(palavras, stream.Text) { //Aqui só grava no banco se conter uma string que eu passar
				// Por exemplo, quero buscar os cupons que tem a string "Kindle"

				a := &DynamoItem{stream.ID, stream.Text, stream.CreateAt.In(loc)} //seta os dados (id, text, created_at) na na struct DynamoItem{}

				av, err := dynamodbattribute.MarshalMap(a) //Faz o marshal no item
				if err != nil {                            //debug
					log.Fatalf("Got error marshalling new movie item: %s", err)
				}

				input := &dynamodb.PutItemInput{ // define o putItem
					Item:                av,
					TableName:           aws.String(tableName),
					ConditionExpression: aws.String("attribute_not_exists(id)"), //Aqui é uma condição, só grava no banco se o ID NÃO existir
				}
				_, errInsert := svc.PutItem(input) //Faz o insert
				if errInsert != nil {              //quando já existe o item, ele exibe o erro
					fmt.Println("Got error calling PutItem: %s", err)
				} else { //se o insert for feito com sucesso, envia a msg
					SendMessageTelegram("Tweet id:" + a.ID + " -\n\n" + a.Text)
					//#### Debug - printa na tela qual o Tweet Id inserido
					fmt.Println("Msg telegram enviada e Gravando o tweet:", a.ID)
				}

			}

		}
		//###### o FOR anterior foi a primeira query rodada, depois dela vamos fazer querys subsequentes, as subsequentes só serão
		// feitas se o o horário do Tweet mais novo da query for menor que X HORAS definidas no último if abaixo, ou seja,
		// Se o Tweet for muito antigo, não queremos ele, e então o FOR para com um Break!

		// Aqui se define outra var, a "query 2", ela vai ser rodada passando o ID do tweet mais antigo da quuery anterior.
		query2 := doQuery(complete_struct.Meta.OldestID)
		//###### Debug
		//fmt.Println("#################### ID mais novo:", complete_struct.Meta.OldestID)
		//fmt.Println("#################### QUERY 2", string(query2))

		if err := json.Unmarshal([]byte(query2), &complete_struct); err != nil { // igual ao unmarshal do For anterior
			log.Println(err) // se = erro printa
		}

		t1 = time.Now().In(loc) //pega a hora atual com o Time Zone de SP

		//###### Debug
		// fmt.Println("Hora atual:", t1) //debug
		// fmt.Println("Hora do Tweet do Count:", GetDateTweet(complete_struct.Meta.NewestID)) //debug

		// Faz a diferença da hora atual para a do Tweet mais novo da query,
		// se esse Tweet foi criado a mais de x horas, não quermos ele, então BREAK!
		diff := t1.Sub(GetDateTweet(complete_struct.Meta.NewestID))
		if diff.Minutes() > 30 {
			break
		}

	}

}

//########################## Retorna a data do tweet passado por parametro, essa data vai ser comparada com a data atual,
// Se o Tweet for muito antigo, repito, não queremos ele. Essa data é usada no método anterior.
func GetDateTweet(id string) time.Time {
	client := &http.Client{}
	// bearer_token, err := os.ReadFile("/home/user/projetos/gitlab.com/pnkfd/cupom/pass") // pega o BEARER TOKEN do twitter
	// if err != nil {
	// 	log.Println(err)
	// }
	query := "https://api.twitter.com/2/tweets/"
	query += id + "?"
	query += "tweet.fields=created_at"

	req, _ := http.NewRequest("GET", query, nil)
	req.Header.Add("Authorization", "Bearer "+string(bearer)) //seta o header
	//fmt.Println(string(bearer_token)) //debug
	resp, _ := client.Do(req) // Faz a requisição
	defer resp.Body.Close()   //fecha depois

	body, _ := ioutil.ReadAll(resp.Body) // Lê a resposta
	sb := string(body)                   // Converte a resposta para string
	//fmt.Println(sb) // imprime - Para debug
	single := SingleTweet{} // inicializa a struct que vai ser enviada para o DYNAMMO

	if err := json.Unmarshal([]byte(sb), &single); err != nil {
		log.Println(err) // se = erro printa
	}
	return single.Data.CreatedAt.In(loc)
}

//########### Aqui é o inicio da requisição, fazemos uma query buscando por "cupom de desconto",
// sempre vai me retornar os Tweets mais novos. Esse método serve apenas para me retornar o Tweet mais novo, o ID dele.

func GetMaisNovo() string {
	client := &http.Client{}
	// bearer_token, err := os.ReadFile("/home/user/projetos/gitlab.com/pnkfd/cupom/pass") // pega o BEARER TOKEN do twitter
	// if err != nil {
	// 	log.Println(err)
	// }

	//######################## Builda a Query
	query := "https://api.twitter.com/2/tweets/search/recent?&max_results=10&"
	query += "query=" + url.QueryEscape("(Cupom desconto has:media OR Cupom desconto) -pack -p4ck -is:retweet  -is:reply -from:cupom")

	req, _ := http.NewRequest("GET", query, nil)
	req.Header.Add("Authorization", "Bearer "+string(bearer)) //seta o header
	//fmt.Println(string(bearer_token)) //debug
	resp, _ := client.Do(req) // Faz a requisição
	defer resp.Body.Close()   //fecha depois

	body, _ := ioutil.ReadAll(resp.Body) // Lê a resposta
	sb := string(body)                   // Converte a resposta para string
	single := Autogenerated{}            // inicializa a struct que vai ser enviada para o DYNAMMO

	if err := json.Unmarshal([]byte(sb), &single); err != nil {
		log.Println(err) // se = erro printa
	}
	//fmt.Println("##########################3 ", single.Meta.NewestID)
	return single.Meta.NewestID
}

//######## Nesse método iremos enviar uma mensagem para o telegram toda vez que um cupom for encontrado
// o método recebe uma string, que é o texto a ser enviado pelo bot
func SendMessageTelegram(text string) {
	//Aqui eu coloco um QueryScape para não dar problema na URL, pois tem caracteres inválidos nos tweets
	resp, err := http.Get("https://api.telegram.org/" + bot_id + "/sendMessage?chat_id=" + chat_id + "&text=" + url.QueryEscape(text))

	if err != nil { //se der erro, "loga"
		log.Print(err)
	}
	defer resp.Body.Close() //fecha a requisição
}

//##### Método para verificar se no Tweet lido tem alguma das strings que queremos, essas strings serão palavras
// chaves utilizadas para buscar informações no tweet, por exemplo: kindle, amazon, delivery, ifood
func contains(array, tweet string) bool {
	s := strings.Split(array, " ") //faz a string virar um arrray
	for _, a := range s {          // passa esse array no for
		//	fmt.Println(a) //debug
		if strings.Contains(tweet, a) {
			fmt.Println("contém a palavra: ", a) //debug
			return true
		}
	}
	return false
}
