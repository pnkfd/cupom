package models

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type Item struct {
	Id         string    `json:"id"`
	Created_at time.Time `json:"created_at"`
	Text       string    `json:"text"`
}

func SearchAll() []Item {

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	params := &dynamodb.ScanInput{
		TableName: aws.String("cupom"),
	}
	result, err := svc.Scan(params)
	if err != nil {
		fmt.Errorf("failed to make Query API call, %v", err)

	}

	obj := []Item{}
	dynamodbattribute.UnmarshalListOfMaps(result.Items, &obj)

	//b, _ := json.Marshal(obj)
	return obj

}
